package pl.sdacademy.zadanieDomowe.spiralnaTablica;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);

    private static boolean getBoolean() {
        String string = scanner.next();
        if (string.equals("Y") || string.equals("y")) {
            return true;
        } else {
            return false;
        }
    }

    private static int setHeight() {
        System.out.println("Enter height: ");
        return getNumber();
    }

    private static boolean clockwais() {
        System.out.println("As clockwais? Y/N");
        return getBoolean();
    }

    private static boolean grow() {
        System.out.println("grow? Y/N");
        return getBoolean();
    }

    private static int setWight() {
        System.out.println("Enter wight: ");
        return getNumber();
    }

    private static int getNumber() {
        int number = scanner.nextInt();
        return number;
    }

    public static void main(String[] args) {
        Table table = new Table(setWight(), setHeight());
        table.setTable(Spiral.selectGenerete(table.getTable(), clockwais(), grow()));
        table.printTable();
    }

}
